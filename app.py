from time import strftime
from flask import Flask, render_template, flash, request
from wtforms import Form, validators, DecimalField

from data_base import DataForTable
from db_utils import SessionContextManager
import numpy as np


DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = 'SjdnUends821Jsdlkvxh391ksdODnejdDw'


class ReusableForm(Form):
    value_1 = DecimalField('Value 1', validators=[validators.required()])
    value_2 = DecimalField('Value 2', validators=[validators.required()])
    value_3 = DecimalField('Value 3', validators=[validators.required()])
    value_4 = DecimalField('Value 4', validators=[validators.required()])
    value_5 = DecimalField('Value 5', validators=[validators.required()])


def get_time():
    time = strftime("%Y-%m-%dT%H:%M")
    return time


def calculate_table_data(session, value_1=0, value_2=0, value_3=0, value_4=0, value_5=0):
    all_rows = session.query(DataForTable).all()

    table_data = []

    _value_1 = float(value_1)
    _value_2 = float(value_2)
    _value_3 = float(value_3)
    _value_4 = float(value_4)
    _value_5 = float(value_5)

    index = 0
    for row in all_rows:
        index += 1
        result = np.round(
            row.x1 * _value_1 + row.x2 * _value_2 + row.x3 * _value_3 + row.x4 * _value_4 + row.x5 * _value_5, 3)
        table_row = {'index': index,
                     'value_1': row.x1,
                     'value_2': row.x2,
                     'value_3': row.x3,
                     'value_4': row.x4,
                     'value_5': row.x5,
                     'result': result}
        table_data.append(table_row)

    return table_data


@app.route("/", methods=['GET', 'POST'])
def hello():
    scm = SessionContextManager()
    with scm.session_scope() as session:
        form = ReusableForm(request.form)
        table_data = calculate_table_data(session, 0, 0, 0, 0, 0)

        if request.method == 'POST':
            value_1 = request.form['value_1']
            value_2 = request.form['value_2']
            value_3 = request.form['value_3']
            value_4 = request.form['value_4']
            value_5 = request.form['value_5']

            if (value_1 != '') & (value_2 != '') & (value_3 != '') & (value_4 != '') & (value_5 != ''):
                flash('You submit values: {}; {}; {}; {}; {} '.format(value_1, value_2, value_3, value_4, value_5))
                table_data = calculate_table_data(session, value_1, value_2, value_3, value_4, value_5)

            else:
                flash('Error: All Fields are Required')

        return render_template('index.html', form=form, table_data=table_data)


if __name__ == "__main__":
    app.run()
