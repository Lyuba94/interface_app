Инструкция по разворачиванию приложения

1) Установить conda env с помощью команды:

    conda env create -f environment.yml
    
    conda activate interface_app
    
    // conda env export --name interface_app > environment.yml

2) Создать отдельную базу example в postgres  (если база другая или дркгие настройки то нужно отразить это в config.ini)
 
    python data_base.py

3) Сгенерировать значения с помощью скрипта

    python generate_data_into_db.py

4) Запустить flask приложение

    python app.py
