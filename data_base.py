from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float
from config_utils import Config
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import logging
Base = declarative_base()


class DataForTable(Base):
    """
    Класс для данных из таблицы, которые будут отображаться на фронтенд
    """
    __tablename__ = 'data_for_table'
    id = Column(Integer, primary_key=True)
    x1 = Column(Float)
    x2 = Column(Float)
    x3 = Column(Float)
    x4 = Column(Float)
    x5 = Column(Float)

    def __init__(self, x1, x2, x3, x4, x5):
        self.x1 = x1
        self.x2 = x2
        self.x3 = x3
        self.x4 = x4
        self.x5 = x5

    def __repr__(self):
        return "<DataForTable('%s','%s', '%s', '%s', '%s')>" % (str(self.x1),
                                                                str(self.x2),
                                                                str(self.x3),
                                                                str(self.x4),
                                                                str(self.x5),)


if __name__ == "__main__":
    config = Config()
    logging.basicConfig(filename="logs.log", level=logging.INFO)
    s = 'postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}'.format(DB_USER=config.db_user,
                                                                                   DB_PASSWORD=config.db_password,
                                                                                   DB_NAME=config.db_name,
                                                                                   DB_HOST=config.db_host)
    engine = create_engine(s, echo=True)
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()
    logging.info("Database created!")