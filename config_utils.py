# https://www.hackerearth.com/ru/practice/notes/samarthbhargav/a-design-pattern-for-configuration-management-in-python/
import os.path as path
import configparser


class Config(object):
    """
     Class for parsing configs for different modes or machines
    """

    def __init__(self, path_to_configs='config.ini'):
        # If mode doesn't exists in configs set TEST MODE
        self._config = configparser.ConfigParser()
        self._config.read(path_to_configs)
        # self.mode = self._config['MODE'].setdefault('MODE', 'TEST')

    def get_property(self, property_name):
        if property_name not in (self._config['database']).keys():  # we don't want KeyError
            return None  # just return None if not found
        return self._config['database'][property_name]

    @property
    def db_user(self):
        return self.get_property('DB_USER')

    @property
    def db_password(self):
        return self.get_property('DB_PASSWORD')

    @property
    def db_name(self):
        return self.get_property('DB_NAME')

    @property
    def db_host(self):
        return self.get_property('DB_HOST')


if __name__ == "__main__":
    config = Config()

