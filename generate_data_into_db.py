from data_base import DataForTable
from db_utils import SessionContextManager
import numpy as np
import logging


def generate_data():
    """
    Функция для генерации случайных значений в базу
    """
    logging.basicConfig(filename="logs.log", level=logging.INFO)
    scm = SessionContextManager()
    with scm.session_scope() as session:
        for i in range(3):
            x1 = np.round(np.random.sample(), 2)
            x2 = np.round(np.random.sample(), 2)
            x3 = np.round(np.random.sample(), 2)
            x4 = np.round(np.random.sample(), 2)
            x5 = np.round(np.random.sample(), 2)
            data_example = DataForTable(x1=x1, x2=x2, x3=x3, x4=x4, x5=x5)
            session.merge(data_example)

        session.flush()
        session.commit()
    logging.info("Values for database created!")


if __name__ == "__main__":
    generate_data()
