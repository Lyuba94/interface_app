from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from config_utils import Config


# https://docs.sqlalchemy.org/en/13/orm/session_basics.html


class SessionContextManager(object):
    def __init__(self):
        self.config = Config()

    def get_sql_session(self):
        s = self.open_sql_session(DB_USER=self.config.db_user,
                                  DB_PASSWORD=self.config.db_password,
                                  DB_NAME=self.config.db_name,
                                  DB_HOST=self.config.db_host)
        return s

    @contextmanager
    def session_scope(self):
        """Provide a transactional scope around a series of operations."""
        session = self.get_sql_session()

        try:
            yield session
            session.commit()
        except:
            session.rollback()

            raise
        finally:
            session.close()

    @staticmethod
    def open_sql_session(DB_USER, DB_PASSWORD, DB_NAME, DB_HOST):
        """
        Fuction to create any session
        :param DB_USER:
        :param DB_PASSWORD:
        :param DB_NAME:
        :param DB_HOST:
        :return:
        """
        s = 'postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}'.format(DB_USER=DB_USER,
                                                                                       DB_PASSWORD=DB_PASSWORD,
                                                                                       DB_NAME=DB_NAME,
                                                                                       DB_HOST=DB_HOST)
        # engine = create_engine(s, echo=True)
        engine = create_engine(s, echo=False)
        Session = sessionmaker(bind=engine)
        session = Session()
        return session

    def get_sql_connection(self):
        s = 'postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}'.format(DB_USER=self.config.db_user,
                                                                                       DB_PASSWORD=self.config.db_password,
                                                                                       DB_NAME=self.config.db_name,
                                                                                       DB_HOST=self.config.db_host)
        engine = create_engine(s, echo=True)
        connection = engine.connect()
        return connection


if __name__ == "__main__":
    scm = SessionContextManager()
    scm.get_sql_connection()

    with scm.session_scope() as session:
        print('123')
